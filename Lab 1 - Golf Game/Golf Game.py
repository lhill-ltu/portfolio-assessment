import sys, pygame, random, math, time

## Setup code

pygame.init()

## Initialise all variables here

screen = pygame.display.set_mode((500,500))

a = random.randint(0, 500) 
b = random.randint(0, 500)
noCollision = False

#From Stack Overflow, changed to stay 6 pixels away from the objects so the hole isn't half in the block
def collision(rectLeft, rectTop, width, height, centerX, centerY, radius): #Detection for the random object on the map, will not spawn within certain range of both the ball and the hole
    rectRight = rectLeft + width/2
    rectBottom = rectTop + height/2

    circLeft = centerX - radius
    circRight = centerX + radius
    circTop = centerY - radius
    circBottom = centerY + radius


    #Check if the rectangle object and the circle objects are not colliding
    if(rectRight < (circLeft - 6) or rectLeft > (circRight + 6) or rectBottom < (circTop - 6) or rectTop > (circBottom + 6)):
        return False # No collision

    #Check if any point of the rectangle is inside the circle
    for x in (rectLeft, rectLeft + width):
        for y in (rectTop, rectTop + height):
            #Compare the distance between the centre of the circle and the point to the rectangle, if it is less that the radius then the point is inside the circle
            if(math.hypot(x - centerX, y - centerY) <= radius):
                return True # Collision

    #Check if the centre of the circle is inside the rectangle
    if(rectLeft < centerX < rectRight and rectTop < centerY < rectBottom):
        return True

    return True


game_won = False
game_over = False

x = 100
y = 100
dx = 0
dy = 0

currentLives = 3

aiming = True
# Game loop

while True:
    # Event detection
    # Model Update
    targetX = a #targetX is the x coordinate of the hole
    targetY = b #targetY is the y coordinate of the hole
    x = x + dx # update the x position
    y = y + dy # update the y position

    if(x > 500 or x < 0):
        dx *= -1

    if(y > 500 or y < 0):
        dy *= -1

    # if((x > u and x < u + w) or (y > k and y < k + h)):
    #     dx *= -1
    #     dy *= -1

    dx *= 0.999
    dy *= 0.999

    if((targetX - x) <= 10 and (targetX - x) >= -10):
        if((targetY - y) <= 10 and (targetY - y) >= -10):
            game_won = True

    if(game_won == True):
        dy = 0
        dx = 0
        print("Congrats, you won!")

    if(currentLives == 0):
        game_over = True
        dy = 0
        dx = 0
        print("You lose :(")

    # This is necessary for being able to close the game
    for event in pygame.event.get():
        if event.type == pygame.QUIT: 
            sys.exit()
        
        if event.type == pygame.MOUSEBUTTONUP:
            pos =pygame.mouse.get_pos()
            x1 = pos[0]
            y1 = pos[1]
            disX = x1 - x
            disY = y1 - y
            c1 = x - x1
            c2 = y - y1
            inCircle = math.sqrt((c1*c1) + (c2*c2))
            if(aiming == True):
             if(inCircle < 100):
                    aiming = False
                    dx = disX / 15
                    dy = disY / 15

    
    # All drawing code goes here
    if (aiming == False):
        if(game_over == False and game_won == False):
            if(abs(dx) < 0.01 and abs(dy) < 0.01):
                dx = 0
                dy = 0
                currentLives -= 1
                print("You have " + str(currentLives) + " remaining!")
    if(dx == 0 and dy == 0):
                aiming = True
    screen.fill((0,0,0))

    
    for i in range(50):
        for j in range(50):
            pygame.draw.circle(screen,(0, 200, 0),(4 + (i * 10),4 + (j * 10)), 4.5) #Grid
            
    pygame.draw.circle(screen,(0,0,0),(a, b),10) 
    if(game_won == False and game_over == False):
        pygame.draw.circle(screen,(255,255,255),(x,y),10) 
    
    while noCollision == False:
        u = random.randint(0, 500) #X coordinate of the rectangle object
        k = random.randint(0, 500) #Y coordinate of the rectangle object
        w = random.randint(50, 200) #Width of the rectangle object
        h = random.randint(50, 200) #Height of the rectangle object

        if collision(u, k, w, h, x, y, 10) == False:
            if collision(u, k, w, h, a, b, 10) == False:
                noCollision = True
            else:
                noCollision = False
        else:
            noCollision = False

    if noCollision == True:
        pygame.draw.rect(screen,(255,0,0),(u,k,w,h)) #Draw the rectangle object

    if((x > u and x < u + w) and (y > k and y < k + h)): #If the ball collides with the rectangle object
        
        dx *= -1
        dy *= -1

    if(game_won == True):
        pygame.draw.circle(screen,(0,0,255),(x,y),10) #Colour ball blue if won
        font = pygame.font.SysFont(None, 50)
        img = font.render('You Won!', True, (0,255,255))
        screen.blit(img, (180, 180))

    if(game_over == True):
        pygame.draw.circle(screen,(255,0,0),(x,y),10) #Colour ball red if lost
        font = pygame.font.SysFont(None, 50)
        img = font.render('You Lose!', True, (255,0,0))
        screen.blit(img, (175, 180))
    if aiming == True:
        pygame.draw.circle(screen,(125,125,125),(x,y),100, 5) #Aim circle
    
    font = pygame.font.SysFont(None, 24)
    img = font.render('You have ' + str(currentLives) + ' shots remaining!', True, (255,255,255))
    screen.blit(img, (10, 10))

    pygame.display.update()

    if(game_won == True or game_over == True):
        time.sleep(3)
        sys.exit()
