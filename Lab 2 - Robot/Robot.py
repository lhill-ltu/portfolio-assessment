import random

#Make a list of names

names = ['John', 'Paul', 'George', 'Bob', 'Dave']
#chose a random name
a = random.randint(0,4)
name = names[a]
age = random.randint(1,100)
battery = random.randint(1,100)
onoff = random.randint(0,1)
print("Hello, I am a robot named", name)
print("I am", age, "years old")
print("My battery level is", battery, "%")
choice = 0

if onoff == 1:
    print("I am on")
else:
    print("I am off")

while True:
    while choice == 0:
        print("What would you like me to do?")
        print("1. Turn on/off \n2. Charge \n3. Guess a number \n4. Show date and time \n5. Exit")
        choice = int(input("Enter your choice: "))
        
    while choice == 1:
        if onoff == 1:
            print("I am turning off")
            onoff = 0
        else:
            print("I am turning on")
            onoff = 1
        choice = 0

    while choice == 2:
        #Charge a random amount
        if battery != 100:
            charge = random.randint(0, 100 - battery)
            battery = battery + charge
            print("I am charging", charge, "%")
            print("My battery level is", battery, "%")
            choice = 0
        else:
            print("I am already fully charged")
            choice = 0

    while choice == 3:
        randomNumber = random.randint(1,10)
        tries = 3
        print("I am thinking of a number between 1 and 10")
        guess = int(input("Enter your guess: "))
        while guess != randomNumber and tries > 1:
            tries = tries - 1
            print("Wrong, you have", tries, "tries left")
            guess = int(input("Enter your guess: "))
        if guess == randomNumber:
            print("You guessed correctly")
            choice = 0
        else:
            print("You ran out of tries")
            choice = 0

    while choice == 4:
        import datetime
        now = datetime.datetime.now()
        print("The date and time is", now.strftime("%Y-%m-%d %H:%M:%S"))
        choice = 0

    while choice == 5:
        print("Goodbye")
        break
    if choice > 5:
        print("Invalid choice")
        choice = 0
    
    if choice == 5:
        break
        