def check_range(inpString):
    inpString = inpString.lower()
    checkCharacters = True
    for char in inpString:
        i = char
        if ((ord(i) > 122) or (ord(i) < 97 )):
            checkCharacters = False
        if (i == " "):
                checkCharacters = True
    return checkCharacters

def encrypt(inpString, num):
    encryptText = ""

    for char in inpString:
        if(check_range(inpString) == True):
            
            stayInRange = ord(char) + num
            if(stayInRange > 122):
                stayInRange = stayInRange - 26
            resultLetter = chr(stayInRange)
            if (char == ' '):
                resultLetter = ' '
            encryptText = encryptText + resultLetter
    return encryptText

def decrypt(inpString, num):
    decryptText = ""
    for char in inpString:
        if(check_range(inpString) == True):
            stayInRange = ord(char) - num
            if(stayInRange < 97):
                stayInRange = stayInRange + 26
            resultLetter = chr(stayInRange)
            if (char == ' '):
                resultLetter = ' '
            decryptText = decryptText + resultLetter
    return decryptText

#ask if they want to encrypt or decrypt
print("Would you like to encrypt or decrypt a message? \n1. Encrypt \n2. Decrypt")
choice = int(input("Enter 1 or 2: "))
while choice != 1 and choice != 2:
    print("Invalid choice")
    choice = int(input("Enter 1 or 2: "))
#ask for the message
message = input("Enter the message: ")
#ask for the number
num = int(input("Enter the number: "))
while num > 26:
    num = num - 26
while num < 0:
    num = num + 26
#encrypt or decrypt the message
if choice == 1:
    print("The encrypted message is", encrypt(message, num))
else:
    print("The decrypted message is", decrypt(message, num))


