product_names = ["Water", "Liquid sugar", "Zero taste"]
prices = [1.50, 2.00, 2.50]
coins = [2.00, 1.00, 0.50, 0.20, 0.10, 0.05]
pricesSign = ["£1.50", "£2.00", "£2.50"]

while True:
    #print the products
    for i in range(len(product_names)):
        print(i+1, product_names[i],pricesSign[i])
    print("Press X to exit")
    #ask for product
    product = input("Select product: ")
    if product == "x": break
    product = int(product)
    #ask for money
    money = 0
    while money < prices[product-1]:
        print("Please select what coins to use: ")
        for i in range(len(coins)):
            print(i+1, coins[i])
        print("Press X to exit")
        coin = input("Insert coin: ")
        if coin == "x": break
        coin = int(coin)
        money += coins[coin-1]
        print("Money:", money)
    #give change
    change = money - prices[product-1]
    print("Change:", change)
    for i in range(len(coins)):
        while change >= coins[i]:
            print(coins[i])
            change -= coins[i]
