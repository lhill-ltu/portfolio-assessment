import requests

result = requests.get("https://api.adviceslip.com/advice/search/e")
data = result.json()

dos = []
temp = []
donts = ["Don", "don", "do not", "Never", "never", "Do not"]

for slip in data["slips"]:
    advice = slip["advice"]
    if any(word in advice for word in donts):
        temp.append(advice)
    else:
        dos.append(advice)

def countWords(string):
    return len(string.split(" "))

def bubbleSort(adviceList):
    for i in range(len(adviceList)):
        for j in range(len(adviceList)-1):
            if (longer(adviceList[j], adviceList[j+1]) == True):
                temp = adviceList[j]
                adviceList[j] = adviceList[j+1]
                adviceList[j+1] = temp
    return adviceList

def longer(a, b):
    if countWords(a) > countWords(b):
        return True
    elif countWords(b) > countWords(a):
        return False
    else:
        if len(a) >= len(b):
            return True
        else:
            return False

print("Hello, I give out advice :D")
choice = int(input("Which sort of advice would you like: \n1. All Advice \n2. All positive advice in alphabetical order \n3. All positive advice by length \n4. All positive advice by word count \n5. All positive advice by number of words and characters \n6. Christmas tree of advice :D \n"))

if (choice == 1):
 for do in dos: #Print unsorted advice
     print(do)

elif (choice == 2):
    dos.sort()
    for do in dos: #Print Alphabetical Advice
      print(do)

elif (choice == 3):
    dos.sort(key = len) #Print Advice by Length
    for do in dos:
      print(do)

elif (choice == 4):
    dos.sort(key = countWords)
    for do in dos: #Print Advice by Word Count
      print(do)

elif (choice == 5):
    bubbleAdviceList = bubbleSort(dos)
    for bubbleAdvice in bubbleAdviceList: #Print bubble sorted with characters included
      print(bubbleAdvice)

elif (choice == 6):
    bubbleAdviceList = bubbleSort(dos)
    for bubbleAdvice in bubbleAdviceList: #Print Christmas tree advice
        print(bubbleAdvice.center(130))

else:
    print("That wasn't an option D:")
